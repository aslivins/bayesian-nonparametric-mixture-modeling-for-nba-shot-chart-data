A = importdata('NJokicDens.csv');

offset = 5;
y0Num = 80;

y01 = linspace(0,200, y0Num);
y01 = (y01 - min(y01) + offset) / (max(y01 - min(y01) + 2*offset));

for i = 1:y0Num
    for j = 1:y0Num
        if(A(i,j) > 0.001)
            A(i,j) = 0.001;
        end
    end
end

A = transpose(A);

figure(1)
surf(y01, y01, A, 'EdgeColor','none','FaceColor','interp','FaceLighting','phong')
xlabel('x')
ylabel('y')
zlabel('Intensity Function')
title('Nikola Jokic ShotChart Density')

figure(2)
surfc(y01, y01, A, 'EdgeColor','none','FaceColor','interp','FaceLighting','phong')
sc = surfc(A, 'EdgeColor','none','FaceColor','interp','FaceLighting','phong');
title('Nikola Jokic ShotChart Density')
ax = gca;
ax.ZLim(2) = 0.0011;
sc(2).ZLocation = 'zmax';