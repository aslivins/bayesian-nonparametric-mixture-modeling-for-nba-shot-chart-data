# Bayesian Nonparametric Mixture Modeling for NBA Shot Chart Data



## Abstract

In this project, we are motivated to study NBA shot data via nonparametric mixtures to model
the intensity function of a non-homogenous poisson process (NHPP). After developing and applying a joint
intensity model to the NBA data, we can generate results using posterior inference on the random parameters, and use these results to evaluate and compare player performance across seasons.
