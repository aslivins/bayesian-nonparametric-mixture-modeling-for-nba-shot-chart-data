#Creator: Adam Slivinsky
#Code: STAT222 Final Project: Blocked Gibbs Sampler for Spatial Poisson Process
#Date: 15 June 2023


#Clear Data Environment
rm(list=ls(all=TRUE))
set.seed(7) 

#Load Libraries
library(spatstat)
library(LaplacesDemon)
library(mvtnorm)
library(e1071)
library(ggplot2)
library(pracma)
library(KernSmooth)

#Functions to update random parameters
update_Z_l <- function(isIn, mu_0, k0, lambda_0, v0, n_j_star, lx1_star, lx2_star){
  if(isIn == FALSE){
    Z_l <- rnorminvwishart(1, mu_0, k0, lambda_0, v0)
  }
  if(isIn == TRUE){
    x_j_star_1 <- mean(lx1_star)
    x_j_star_2 <- mean(lx2_star)
    x_j_star <- c(x_j_star_1, x_j_star_2)
    kn <- k0 + n_j_star
    vn <- v0 + n_j_star
    mu_n = (k0/kn)*mu_0 + (n_j_star/kn)*x_j_star
    lambda_n = lambda_0 + (k0*n_j_star/kn)*(t(x_j_star - mu_0) %*% (x_j_star - mu_0))
    for (h in 1:n_j_star){
      temp_lx <- c(lx1_star[h], lx2_star[h])
      lambda_n = lambda_n + ((temp_lx - x_j_star) %*% t(temp_lx - x_j_star))
    }
    Z_l <- rnorminvwishart(1, mu_n, kn, lambda_n, vn)
  }
  return(Z_l)
}

update_V <- function(alpha, uniques, counts, N){
  V = rep(0, N-1)
  M = rep(0, N)
  ind = 1
  for (l in 1:N){
    if(length(which(uniques == l)) > 0){
      M[l] = counts[ind]
      ind = ind + 1
    }
    else{
      M[l] = 0
    }
  }
  for(l in 1:(N-1)){
    V[l] <- rbeta(1, 1 + M[l], alpha + sum(M[(l+1):N]))
  }
  return(V)
}

update_p <- function(V, N){
  p <- rep(0, N)
  p[1] = V[1]
  for(l in 2:(N-1)){
    temp = V[l]
    for(l2 in 1:(l-1)){
      temp = temp * (1 - V[l2])
    }
    p[l] = temp
  }
  if(sum(p[1:(N-1)]) >= 1){
    p[N] <- 0
    p[1:(N-1)] <- p[1:(N-1)] / sum(p[1:(N-1)])
  }
  else{
    p[N] = 1 - sum(p[1:(N-1)]) 
  }
  return(p)
}

update_L_i <- function(lx, mu, sigma, p, N){
  p_i <- rep(0, N)
  for (l in 1:N){
    p_i[l] <- p[l]*dmvnorm(lx, mu[l,], sigma[,,l])
  }
  p_i <- p_i / sum(p_i)
  L_i <- rdiscrete(1, p_i, values = 1:N)
  return(L_i)
}

update_mu_0 <- function(mu_p, sigma_p, mu, sigma, k0, N){
  sigmaSum = 0
  muSum = 0
  for(l in 1:N){
    sigmaSum = sigmaSum + solve((1/k0)*sigma[,,l])
  }
  
  sigma_1 <- solve(solve(sigma_p) + sigmaSum)
  mu_1 <- sigma_1 %*% ((solve(sigma_p) %*% mu_p) + (sigmaSum %*% colMeans(mu)))
  
  muRNew = rmvnorm(1, mu_1, solve(sigma_1))
  return(muRNew)
  
}

update_alpha <- function(N, a_alpha, b_alpha, V){
  sum_b <- 0
  for (l in 1:(N-1)){
    if(V[l] == 1){
      sum_b = sum_b + log(0.95)
    }
    if(V[l] < 1){
      sum_b = sum_b + log(1-V[l])
    }
  }
  alpha <- rbeta(1,N + a_alpha - 1, b_alpha - sum_b)
  return(alpha)
}

update_y0_dens <- function(points1, N, p, mu, sigma, y01){
  l0 <- rdiscrete(1, p, values = 1:N)
  dens <- array(0, dim = c(points1, points1))
  for (n in 1:(points1)){
    for(n2 in 1:points1){
      dens[n, n2] = dmvnorm(logit(c(y01[n], y01[n2])), mu[l0,], sigma[,,l0])
      dens[n, n2] = dens[n, n2] / (y01[n]*(1-y01[n])*y01[n2]*(1-y01[n2]))
    }
  }
  return(dens)
}

#Testing functions
test1 <- update_Z_l(TRUE, rmvnorm(1, c(0,0), diag(2)), 0.1, diag(2), 4, 10, c(1:10), c(11:20))
test2 <- update_Z_l(FALSE, c(0,0), 0.1, diag(2), 4, 0, 0, 0)

N = 10
L <- rpois(N, 100)
uniques <- sort(unique(L))
counts <- array(table(L), dim = c(length(unique(L))))
test3 <- update_V(5, uniques, counts, N)

test4 <- update_p(test3, N)

Z_test5 <- rnorminvwishart(N, c(0,0), 0.1, diag(2), 4)
test5 <- update_L_i(logit(c(0.25,0.75)), Z_test5$mu, array(rep(Z_test5$Sigma, N), dim=c(2,2,N)), test4, N)

test6 <- update_mu_0(c(0,0), diag(2), Z_test5$mu, array(rep(Z_test5$Sigma, N), dim = c(2,2,N)), 0.1, N)

test7 <- update_alpha(N, 2, 0.9, test4)

points1 <- 80
offset <- 0.25
y01 <- linspace(0,200, points1)
y01 <- (y01 - min(y01) + offset) / (max(y01 - min(y01) + 2*offset))
dens <- update_y0_dens(points1, N, test4, Z_test5$mu, array(rep(Z_test5$Sigma, N), dim = c(2,2,N)), y01) 
image(y01, y01, dens, col = hcl.colors(100, "oslo"), axes = FALSE)
contour(y01, y01, dens, levels = seq(min(dens), max(dens), length.out = 10),
        add = TRUE, col = "black")
axis(1, at = seq(0, 200, by = 25))
axis(2, at = seq(0, 200, by = 25))
box()
title(main = "y0 testing density", font.main = 4)




#Load sample data
x1 = longleaf$x
x2 = longleaf$y
n = length(x1)
xDim = 2

#Transform data to fit within (0,1)x(0,1), then logit transform, offset clever!
offset = 0.25
x1 = (x1 - min(x1) + offset) / (max(x1 - min(x1) + 2*offset))
x2 = (x2 - min(x2) + offset) / (max(x2 - min(x2) + 2*offset))
lx1 = logit(x1)
lx2 = logit(x2)

#Prior density of x
ggplot(data.frame(x=x1,y=x2), aes(x=x, y=y) ) +
  geom_point()
xy1 <- cbind(x1, x2)
xy_dis <- bkde2D(xy1, bandwidth = c(dpik(x1), dpik(x2)))
image(xy_dis$x1, xy_dis$x2, xy_dis$fhat, col = hcl.colors(100, "oslo"), axes = TRUE)
contour(xy_dis$x1, xy_dis$x2, xy_dis$fhat, levels = seq(min(xy_dis$fhat), max(xy_dis$fhat), length.out = 10),
        add = TRUE, col = "black")

#Prior density of lx
ggplot(data.frame(x=lx1,y=lx2), aes(x=x, y=y) ) +
  geom_point()

xy1 <- cbind(lx1, lx2)
xy_dis <- bkde2D(xy1, bandwidth = c(dpik(lx1), dpik(lx2)))
image(xy_dis$x1, xy_dis$x2, xy_dis$fhat, col = hcl.colors(100, "oslo"), axes = FALSE)
contour(xy_dis$x1, xy_dis$x2, xy_dis$fhat, levels = seq(min(xy_dis$fhat), max(xy_dis$fhat), length.out = 10),
        add = TRUE, col = "black")




#Set up MCMC parameters and matrices
chains <- 1
iter <- 1000
N <- 50
y0Num <- 80

#Save posterior values in organized format
post <- NULL
post$L <- array(0, dim=c(chains, iter, n))
post$mu <- array(0, dim=c(chains, iter, xDim, N))
post$sigma <- array(0, dim = c(chains, iter, xDim, xDim, N))
post$p <- array(0, dim = c(chains, iter, N))
post$mu_0 <- array(0, dim = c(chains, iter, xDim))
post$y_0_dens <- array(0, dim = c(chains, iter, y0Num, y0Num))
post$n_star <- array(0, dim = c(chains, iter))
post$alpha <- array(0, dim = c(chains, iter))

#Hyperparameter values
mu_p <- c(0,0)
sigma_p <- diag(2)*3
k0 <- 70
v0 <- 24
lambda_0 <- diag(2)
a_alpha <- 2
b_alpha <- 0.9
y01 <- linspace(0,200, y0Num)
y01 <- (y01 - min(y01) + offset) / (max(y01 - min(y01) + 2*offset))

# Start the clock!
ptm <- proc.time()

#MCMC with chains
for (m in 1:chains){
  
  #Set arbitrary initial values
  alpha <- 5
  mu_0 <- rmvnorm(1, mu_p, sigma_p)
  Z_init <- rnorminvwishart(N, mu_0, 0.01, lambda_0, 4)
  mu <- Z_init$mu
  sigma <- array(rep(Z_init$Sigma, N), dim=c(2,2,N))
  p <- rep(1, N) / N
  
  for(t in 1:iter){
    
    #update L_i
    L <- rep(0, length(x1))
    for (i in 1:length(x1)){
      L[i] <- update_L_i(c(lx1[i], lx2[i]), mu, sigma, p, N)
    }
    
    #update V
    uniques <- sort(unique(L))
    counts <- array(table(L), dim = c(length(unique(L))))
    V <- update_V(alpha, uniques, counts, N)
    
    #update alpha
    alpha <- update_alpha(N, a_alpha, b_alpha, V)
    
    #update p
    p <- update_p(V, N)
    
    #update Z_l
    ind_Z <- 1
    for (j in 1:N){
      if(length(which(uniques == j)) > 0){
        xInd <- which(L == j)
        lx1_temp <- array(0, dim = c(length(xInd)))
        lx2_temp <- array(0, dim = c(length(xInd)))
        for(k in 1:(length(xInd))){
          lx1_temp[k] <- lx1[xInd[k]]
          lx2_temp[k] <- lx2[xInd[k]]
        }
        Z_l <- update_Z_l(TRUE, mu_0, k0, lambda_0, v0, counts[ind_Z], lx1_temp, lx2_temp)
        mu[j,] <- Z_l$mu
        sigma[,,j] <- Z_l$Sigma
        ind_Z <- ind_Z + 1
      }
      if(length(which(uniques == j)) == 0){
        Z_l <- update_Z_l(FALSE, mu_0, k0, lambda_0, v0, 0, 0, 0)
        mu[j,] <- Z_l$mu
        sigma[,,j] <- Z_l$Sigma
      }
    }
    
    #update mu_0
    mu_0 <- update_mu_0(mu_p, sigma_p, mu, sigma, k0, N)
    
    #update y0_dens
    l_temp <- rdiscrete(1, p, 1:N)
    y0_dens <- update_y0_dens(y0Num, N, p, mu, sigma, y01)
    image(y01, y01, y0_dens, col = hcl.colors(100, "oslo"), axes = FALSE)
    
    
    #Save values
    post$L[m,t,] <- L
    post$mu[m,t,,] <- mu
    post$sigma[m,t,,,] <- sigma
    post$p[m,t,] <- p
    post$mu_0[m,t,] <- mu_0
    post$y_0_dens[m,t,,] <- y0_dens
    post$n_star[m,t] <- length(uniques)
    post$alpha[m,t] <- alpha
  }
}

# Stop the clock
proc.time() - ptm

burn = 800
#Posterior mu, y0_dens density
mu_post <- post$mu[1,(burn+1),,]
y0_dens_post <- post$y_0_dens[1,(burn+1),,]
for(m1 in 1:chains){
  for(m2 in (burn + 2):iter){
    mu_post <- rbind(mu_post, post$mu[m1, m2, ,])
    y0_dens_post <- y0_dens_post + post$y_0_dens[m1, m2,,]
  }
}

# Normalize y0_dens (1/B)
y0_dens_post <- y0_dens_post / sum(y0_dens_post)

#plot Posterior mu
mu_postdens <- bkde2D(mu_post, bandwidth = 1.5* c(dpik(mu_post[,1]), dpik(mu_post[,2])))
contour(mu_postdens$x1, mu_postdens$x2, mu_postdens$fhat, main = 'Posterior mu MCMC')

#Plot Posterior y0_dens
image(y01, y01, y0_dens_post, col = hcl.colors(100, "oslo"), axes = FALSE)
contour(y01, y01, y0_dens_post, levels = seq(min(y0_dens_post), max(y0_dens_post), length.out = 10),
        add = TRUE, col = "black")
axis(1, at = linspace(0,1, 7))
axis(2, at = linspace(0,1, 7))
box()
title(main = "y0 Posterior Density", font.main = 4)
  
  
  